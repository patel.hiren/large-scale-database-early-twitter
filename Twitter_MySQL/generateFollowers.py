#!/usr/bin/python

import random
from random import randrange
import string

import csv
with open("generatedFollowers.csv", "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')

        for user_id in range(15000):
            numFollowing = randrange(50)
            while numFollowing > 0:
                follows_id = randrange(15000)
                while user_id == follows_id:
                    follows_id = randrange(15000)
                csvRow = [user_id, follows_id]
                writer.writerow(csvRow)
                numFollowing -= 1
