import mysql.connector
import csv
import time

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="hpatelPass123$",
  database="twitter_hw1"
)

mycursor = mydb.cursor()

t0 = time.time()
with open('generatedTweets.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        sql = "INSERT INTO tweets (tweet_id, user_id, tweet_ts, tweet_text) VALUES (%s, %s, %s, %s)"
        val = (row['tweet_id'], row['user_id'], row['tweet_ts'], row['tweet_text'])
        mycursor.execute(sql, val)
mydb.commit()
t1 = time.time()
total = t1-t0

# print(mycursor.rowcount, "record inserted.")
print(total)
