#!/usr/bin/python

import random
from random import randrange
import string
import time

import csv

t0 = time.time()
with open("generatedTweets2.csv", "w", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')

        data = []
        sec = 0;
        min = 0;
        hour = 0;
        day = 1;
        month = 1;
        year = 2020;
        for tweet_id in range(1000000):
            rand_num_tweet_len = randrange(110) + 10
            tweet = ''.join(random.choices(string.ascii_lowercase, k=rand_num_tweet_len))
            user_id = randrange(15000)
            if sec >= 60:
                sec = 0
                min += 1
            if min >= 60:
                min = 0
                hour += 1
            if hour >= 24:
                hour = 1
                day += 1
            if day >= 15:
                print("This took longer than 15 days, redo math.")
            datetime = f"{year}-{month}-{day} {hour}:{min}:{sec}"
            csvRow = [tweet_id, user_id, datetime, tweet]
            writer.writerow(csvRow)
            sec += 1


t1 = time.time()
total = t1-t0

print(total)
