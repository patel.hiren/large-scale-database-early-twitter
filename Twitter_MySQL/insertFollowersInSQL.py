import mysql.connector
import csv
import time

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="hpatelPass123$",
  database="twitter_hw1"
)

mycursor = mydb.cursor()

t0 = time.time()
with open('generatedFollowers.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        sql = "INSERT INTO followers (user_id, follows_id) VALUES (%s, %s)"
        val = (row['user_id'], row['follows_id'])
        mycursor.execute(sql, val)
mydb.commit()
t1 = time.time()
total = t1-t0

# print(mycursor.rowcount, "record inserted.")
print(total)
