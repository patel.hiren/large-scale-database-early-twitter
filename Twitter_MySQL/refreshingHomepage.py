import mysql.connector
import csv
import time

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="hpatelPass123$",
  database="twitter_hw1"
)

mycursor = mydb.cursor()

t0 = time.time()

# add FOR LOOP to get multiple users to test time like 50
iteration = 0
for i in range(15000):
    sql = "SELECT * FROM tweets JOIN followers WHERE followers.user_id = %d AND followers.follows_id = tweets.user_id LIMIT 10;" % i
    mycursor.execute(sql)
    results = mycursor.fetchall()
    if iteration == 100:
        break
    iteration++

t1 = time.time()
total = t1-t0

print(total)
